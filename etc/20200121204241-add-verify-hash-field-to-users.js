module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.addColumn('users', 'verify_hash', {
      type: Sequelize.STRING,
      allowNull: true,
    }),

  down: queryInterface => queryInterface.removeColumn('users', 'verify_hash'),
};
