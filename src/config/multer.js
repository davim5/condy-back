// Configuração do upload de arquivos

// Módulo de multipart form data
import multer from 'multer';
import crypto from 'crypto';
import { extname, resolve } from 'path';

export default {
  // storage: como o multer vai guardar os arquivos
  storage: multer.diskStorage({
    // destination: Pasta de arquivos da aplicação
    destination: resolve(__dirname, '..', '..', 'tmp', 'uploads'),
    // Formatação do nome do arquivo
    filename: (req, file, cb) => {
      /* Adiciona código único antes do nome de cada arquivo
        Para não haver substituição em caso de arquivos com mesmo nome */
      crypto.randomBytes(16, (err, res) => {
        if (err) return cb(err);

        // retorna valor aleatório adcionado da extensão do arquivo
        return cb(null, res.toString('hex') + extname(file.originalname));
      });
    },
  }),
};
