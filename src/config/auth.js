// Configuração do Token (jws)
export default {
  secret: process.env.APP_SECRET,
  expiresIn: '7d',
};
