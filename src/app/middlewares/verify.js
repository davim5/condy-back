// Middleware pra garantir que a conta de usuário é verificada

// Model de user
import User from '../models/User';

export default async (req, res, next) => {
  const { verified } = await User.findByPk(req.userId);

  if (!verified) {
    return res.status(401).json({ error: 'Usuário não verificado!' });
  }

  return next();
};
