// Middleware pra autenticação da sessão atual
import jwt from 'jsonwebtoken';
// Transforma função de 'callback' pra 'async/await'
import { promisify } from 'util';

import authConfig from '../../config/auth';

export default async (req, res, next) => {
  const authHeader = req.headers.authorization;

  if (!authHeader) {
    return res.status(401).json({ error: 'Token não fornecido.' });
  }

  // Token vem como "Bearer eyJhbGci..", então precisa separar
  const [, token] = authHeader.split(' ');

  try {
    // decodifica e separa as informações inseridas no token, como o id
    const decoded = await promisify(jwt.verify)(token, authConfig.secret);

    // Passar o id para o 'req' parar ser reaproveitado e usado nas outras rotas
    req.userId = decoded.id;

    return next();
  } catch (err) {
    return res.status(401).json({ error: 'Token invalido.' });
  }
};
