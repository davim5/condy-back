// Yup: Modulo de validação de schemas
import * as Yup from 'yup';

import User from '../models/User';
import Profile from '../models/Profile';
import Condo from '../models/Condo';

import Mail from '../../lib/Mail';

// Regex pra validar formato do telefone
// const phoneRegExp = /(\(?\d{2}\)?)?(\d{4,5}-\d{4})/;

class UserController {
  // Criar user (do nada)
  async store(req, res) {
    // Esquema de validação dos campos
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      email: Yup.string()
        .email()
        .required(),
      password: Yup.string()
        .required()
        .min(6),
    });

    // Se o body passado for válido
    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Erro na validação.' });
    }

    // Verificação se não já existe email cadastrado
    const userExists = await User.findOne({ where: { email: req.body.email } });

    if (userExists) {
      return res.status(400).json({ error: 'Email de usuário já cadastrado.' });
    }

    // Verificação se não já existe login cadastrado
    const loginExists = await User.findOne({
      where: { login: req.body.login },
    });

    if (loginExists) {
      return res.status(400).json({ error: 'Nome de login já utilizado' });
    }

    const { id, name, email, login } = await User.create(req.body);

    await Mail.sendMail({
      to: `${name} <${email}`,
      subject: `Confirmação de cadastro Condy`,
      template: 'activation',
      context: {
        user: name,
      },
    });

    return res.json({
      id,
      name,
      email,
      login,
    });
  }

  async update(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string(),
      email: Yup.string().email(),
      password: Yup.string()
        .min(6)
        // Se a 'password' for passada, obrigar ter 'oldPassword' preenchida
        .when('oldPassword', (oldPassword, field) =>
          oldPassword ? field.required() : field
        ),
      // Campo de confirmação de senha, garantir que o campo é igual ao 'password'
      confirmPassword: Yup.string().when('password', (password, field) =>
        password ? field.required().oneOf([Yup.ref('password')]) : field
      ),
    });

    // Se o body passado for válido
    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Erro na validação.' });
    }
    const { email, oldPassword } = req.body;

    // Buscar usuário pelo id passado no token(no Middlware auth.js)
    const user = await User.findByPk(req.userId);

    // Se o email estiver sendo alterado(for diferente do atual)
    if (email !== user.email) {
      // Check se o email já existe
      const userExists = await User.findOne({
        where: { email },
      });

      if (userExists) {
        return res.status(400).json({ error: 'Email indisponível para uso.' });
      }
    }

    // Check se o oldPassword bate com a senha atual
    // se ele tiver informado a senha antiga
    if (oldPassword && !(await user.checkPassword(oldPassword))) {
      return res.status(401).json({ error: 'Old Password não confere.' });
    }

    const { id, name, syndical } = await user.update(req.body);

    return res.json({
      id,
      name,
      email,
      syndical,
    });
  }

  // Alterar status do usuário logado para 'Ativo'
  async updateStatusActive(req, res) {
    // Pegar id do usuário pelo token passado no header
    const user = await User.findByPk(req.userId, {
      // Informações para mostrar do Usuário
      attributes: ['id', 'name', 'status'],
    });
    // Alterar status
    await user.update({
      status: 2,
    });

    return res.json(user);
  }

  // Mostrar dados do usuário logado
  async index(req, res) {
    const user = await User.findByPk(req.userId, {
      // Informações para mostrar do Usuário
      attributes: ['id', 'name', 'login', 'status', 'verified'],
      include: [
        // Incluir informações do condomínio do Usuário
        {
          model: Condo,
          as: 'condominium',
          // Informações para mostrar do condomínio
          attributes: ['name', 'adress', 'phone'],
        },
        {
          // Incluir informações do perfil do Usuário
          model: Profile,
          as: 'profile',
          // Informações para mostrar do perfil
          attributes: ['name'],
        },
      ],
    });

    return res.json(user);
  }

  // Deletar usuário pelo id
  async delete(req, res) {
    const user = await User.findByPk(req.body.id);

    await user.destroy();

    return res.json(user);
  }
}

export default new UserController();
