import User from '../models/User';
import File from '../models/File';

// Manipular 'Users' que são síndicos
class SyndicController {
  async index(req, res) {
    // Encontrar Users que são síndicos
    const syndics = await User.findAll({
      where: { syndical: true },
      // Atributos que quero mostrar
      attributes: ['id', 'name', 'email', 'avatar_id'],
      // Adcionar relacionamento com 'File' para mostrar dados do arquivo
      include: [
        {
          model: File,
          as: 'avatar',
          // Atributos que quero mostrar de File
          attributes: ['name', 'path', 'url'],
        },
      ],
    });
    return res.json(syndics);
  }
}

export default new SyndicController();
