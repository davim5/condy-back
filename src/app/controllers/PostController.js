import * as Yup from 'yup';
import Post from '../models/Post';
import User from '../models/User';
import Condo from '../models/Condo';
import Group from '../models/Group';

class PostController {
  // Criar post
  async store(req, res) {
    const schema = Yup.object().shape({
      text: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Erro na validação.' });
    }
    const post = await Post.create(req.body);
    return res.json(post);
  }

  async welcomePost(req, res) {
    // Encontra Usuário logado pelo token
    const user = await User.findByPk(req.userId);

    // Encontra o condomínio do usuário logado
    const condo = await Condo.findByPk(user.condominium_id);

    // Encontra o único grupo criado do condomínio (0000)
    const group = await Group.findOne({
      where: { condominium_id: condo.id },
    });

    // Cria postagem nesse grupo (0000)
    const post = await Post.create({
      text:
        'Bem vindo ao Condy! Comece agora mesmo a interagir com os seus vizinhos desse lugar!',
      type: 1,
      group_id: group.id,
      resident_id: 1,
    });

    return res.json(post);
  }

  async index(req, res) {
    // Encontra Usuário logado pelo token
    const user = await User.findByPk(req.userId);

    // Encontra o condomínio do usuário logado
    const condo = await Condo.findByPk(user.condominium_id);

    // Encontra o único grupo criado do condomínio (0000)
    const group = await Group.findOne({
      where: { condominium_id: condo.id },
    });

    // Encontra todos os posts do grupo encontrado
    const post = await Post.findAll({
      where: { group_id: group.id },
      attributes: [
        'id',
        'type',
        'text',
        'created_at',
        'post_parent',
        'number_like',
      ],
    });

    return res.json(post);
  }
}

export default new PostController();
