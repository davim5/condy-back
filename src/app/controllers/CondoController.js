import * as Yup from 'yup';
import Condo from '../models/Condo';
import User from '../models/User';
// Regex pra validar formato do telefone
const phoneRegExp = /(\(?\d{2}\)?)?(\d{4,5}\d{4})/;

class CondoController {
  // Criar Condomínio
  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      adress: Yup.string().required(),
      phone: Yup.string()
        .required()
        .matches(phoneRegExp),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Erro na validação.' });
    }

    const { id, name, adress, phone, status } = await Condo.create(req.body);
    return res.json({
      id,
      name,
      adress,
      phone,
      status,
    });
  }

  // Atualizar Status do condomínio para ativo
  async updateStatusActive(req, res) {
    // Pega o condomínio do Usuário logado
    // eslint-disable-next-line camelcase
    const { condominium_id } = await User.findByPk(req.userId);

    // Encontra na tabela de condomínio
    const condo = await Condo.findByPk(condominium_id);

    // Altera os status
    await condo.update({
      status: 2,
    });

    return res.json(condo);
  }

  // Mostar dados do condomínio do usuário
  async index(req, res) {
    // Pega o condomínio do Usuário logado
    // eslint-disable-next-line camelcase
    const { condominium_id } = await User.findByPk(req.userId);

    // Encontra na tabela de condomínio
    const condo = await Condo.findByPk(condominium_id);

    return res.json(condo);
  }

  // Deletar condomínio
  async delete(req, res) {
    const condo = await Condo.findByPk(req.body.id);

    await condo.destroy();

    return res.json(condo);
  }
}

export default new CondoController();
