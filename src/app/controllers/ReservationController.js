import * as Yup from 'yup';
import Reservation from '../models/Reservation';
import User from '../models/User';
import Condo from '../models/Condo';
import CommonArea from '../models/CommonArea';

class ReservationController {
  // Criar Reserva
  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Erro na validação.' });
    }

    // COLOCAR PRA CRIAR NAS
    const reservation = await Reservation.create(req.body);

    return res.json(reservation);
  }

  // Mostrar reservas
  async index(req, res) {
    // Encontra Usuário logado pelo token
    const user = await User.findByPk(req.userId);

    // Encontra o condomínio do usuário logado
    const condo = await Condo.findByPk(user.condominium_id);

    // Encontra area comum relacionado ao condomínio encontrado
    const area = await CommonArea.findOne({
      where: { condominium_id: condo.id },
    });

    // Encontra reservas feitas na área comum encontrada
    const reservation = await Reservation.findAll({
      where: { common_area_id: area.id },
    });

    return res.json(reservation);
  }
}

export default new ReservationController();
