import jwt from 'jsonwebtoken';
import { promisify } from 'util';
import 'dotenv/config';
import authConfig from '../../config/auth';
import User from '../models/User';

import Mail from '../../lib/Mail';

class VerifyController {
  async verifyMail(req, res) {
    // Encontra usuário logado na sessão
    const user = await User.findByPk(req.userId);

    // Pega o token enviado na query do link
    const token = req.query.code;

    // Decodifica as informações do token
    const decoded = await promisify(jwt.verify)(token, authConfig.secret);

    // Pega o nome e o email, do usuário que enviou, que estão inseridos no token
    const { name, email } = decoded;

    // Compara o nome e o email do token com o do usuário logado
    // Se bater, atualiza o status de verificado para 'true'
    if (user.name === name && user.email === email) {
      await user.update({
        verified: true,
      });
      return res.json(user);
    }
    return res
      .status(401)
      .json({ error: 'Erro na verificação, envie o email' });
  }

  async sendMail(req, res) {
    // Pega nome e email do usuário na session
    const { name, email } = await User.findByPk(req.userId);

    // Criar token com nome e email do usuários codificados
    const token = jwt.sign({ name, email }, authConfig.secret, {
      expiresIn: '1h',
    });

    // Link com o token sendo passado na query
    const link = `${process.env.APP_URL}/verify?code=${token}`;

    // Envio do email

    try {
      await Mail.sendMail({
        to: `${name} <${email}>`,
        subject: `Confirmação de cadastro Condy`,
        template: 'activation',
        context: {
          user: name,
          link,
        },
      });
      // Retorno com o link do email
      return res.json({ message: 'Email Enviado', link });
    } catch (err) {
      return res.status(400).json({ error: 'Erro no envio do email' });
    }
  }
}

export default new VerifyController();
