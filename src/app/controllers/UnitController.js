import * as Yup from 'yup';
import Unit from '../models/Unit';
import User from '../models/User';
// import User from '../models/User';

class UnitController {
  // Criar unidade do nada
  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Erro na validação.' });
    }
    const unit = await Unit.create(req.body);
    return res.json(unit);
  }

  // Criar unidade pelo Usuário logado (Não implementado)
  async storeByUser(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Erro na validação.' });
    }

    // Encontrar condomínio do usuário logado
    // eslint-disable-next-line camelcase
    const { condominium_id } = await User.findByPk(req.userId);

    // Criar grupo no condomínio encontrado
    const unit = await Unit.create({
      name: req.body.name,
      condominium_id,
    });

    return res.json(unit);
  }
}

export default new UnitController();
