import * as Yup from 'yup';
import CommonArea from '../models/CommonArea';
import User from '../models/User';

// Regex pra validar formato do telefone

class CommonAreaController {
  // Criar Area Comum no logado no Usuário
  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Erro na validação.' });
    }

    // Encontrar o condomínio do usuário logado
    // eslint-disable-next-line camelcase
    const { condominium_id } = await User.findByPk(req.userId);

    // Criar àrea comum no condomínio encontrado
    const area = await CommonArea.create({
      name: req.body.name,
      condominium_id,
    });

    return res.json(area);
  }

  // Mostrar àreas comuns do condomínio do usuários
  async index(req, res) {
    // eslint-disable-next-line camelcase
    const { condominium_id } = await User.findByPk(req.userId);

    const areas = await CommonArea.findAll({
      where: {
        condominium_id,
      },
      attributes: ['id', 'name', 'open_hour', 'close_hour'],
    });

    return res.json(areas);
  }
}

export default new CommonAreaController();
