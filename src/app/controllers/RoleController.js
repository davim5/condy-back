// Yup: Modulo de validação de schemas
import * as Yup from 'yup';
import Role from '../models/Role';

class RoleController {
  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
    });

    // Se o body passado for válido
    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Erro na validação.' });
    }

    // Verificação se não já existe role com mesmo nome
    const profileExists = await Role.findOne({
      where: { name: req.body.name },
    });

    if (profileExists) {
      return res.status(400).json({ error: 'Nome de login já utilizado' });
    }

    const role = await Role.create(req.body);

    return res.json(role);
  }
}

export default new RoleController();
