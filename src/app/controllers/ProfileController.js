// Yup: Modulo de validação de schemas
import * as Yup from 'yup';
import Profile from '../models/Profile';

class ProfileController {
  // Criar Perfil
  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
    });

    // Se o body passado for válido
    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Erro na validação.' });
    }

    // Verificação se não já existe profile com mesmo nome
    const profileExists = await Profile.findOne({
      where: { name: req.body.name },
    });

    if (profileExists) {
      return res.status(400).json({ error: 'Nome de login já utilizado' });
    }

    const profile = await Profile.create(req.body);

    return res.json(profile);
  }

  // Deletar perfil pelo id
  async delete(req, res) {
    const profile = await Profile.findByPk(req.body.id);

    await profile.destroy();

    return res.json(profile);
  }
}

export default new ProfileController();
