import * as Yup from 'yup';
import Group from '../models/Group';
import User from '../models/User';

class GroupController {
  // Criar Grupo no Condomínio do Usuário logado
  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Erro na validação.' });
    }

    // Encontrar condomínio do usuário logado
    // eslint-disable-next-line camelcase
    const { condominium_id } = await User.findByPk(req.userId);

    // Criar grupo no condomínio encontrado
    const group = await Group.create({
      name: req.body.name,
      condominium_id,
    });
    return res.json(group);
  }

  // Mostrar grupos que contém no condomínio do Usuário logado
  async index(req, res) {
    // Encontrar condomínio do usuário logado
    // eslint-disable-next-line camelcase
    const { condominium_id } = await User.findByPk(req.userId);

    // Encontrar todos os grupos relacionados ao condomínio encontrado
    const groups = await Group.findAll({
      where: {
        condominium_id,
      },
    });

    return res.json(groups);
  }
}

export default new GroupController();
