// Módulo do token
import jwt from 'jsonwebtoken';
// Módulo da validação dos campos
import * as Yup from 'yup';
import User from '../models/User';
import authConfig from '../../config/auth';

class SessionController {
  // Criar sessão
  async store(req, res) {
    // Esquema de validação dos campos
    const schema = Yup.object().shape({
      login: Yup.string().required(),
      password: Yup.string().required(),
    });

    // Se o body passado for válido
    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Erro na validação.' });
    }

    // Pegar somente email e senha
    const { login, password } = req.body;

    // Buscar o usuário pelo email
    const user = await User.findOne({ where: { login } });

    // Verificação se usuário foi encontrado pelo email
    if (!user) {
      return res.status(401).json({ error: 'User não encontrado.' });
    }

    // Verificação da senha do usuário
    if (!(await user.checkPassword(password))) {
      return res.status(401).json({ error: 'Senha incorreta.' });
    }

    // Pegar id, nome e email do usuário encontrado
    // eslint-disable-next-line camelcase
    const { id, name, email, created_at, last_activity } = user;

    return res.json({
      user: {
        id,
        name,
        email,
        login,
        created_at,
        last_activity,
      },
      // Gerando Token da sessão
      token: jwt.sign({ id }, authConfig.secret, {
        // Validade do Token
        expiresIn: authConfig.expiresIn,
      }),
    });
  }
}

export default new SessionController();
