import * as Yup from 'yup';
import Employee from '../models/Employee';
import User from '../models/User';

class EmployeeController {
  // Criar Funcionário
  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Erro na validação.' });
    }

    // Encontrar condomínio do usuário logado
    // eslint-disable-next-line camelcase
    const { condominium_id } = await User.findByPk(req.userId);

    // Criar condomínio passando: name, job, e unit_id
    const employee = await Employee.create({
      name: req.body.name,
      job: req.body.job,
      unit_id: req.body.unit_id,
      condominium_id,
    });

    return res.json(employee);
  }
}

export default new EmployeeController();
