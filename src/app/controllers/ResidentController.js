import * as Yup from 'yup';
import Residents from '../models/Resident';

// Regex pra validar formato do telefone

class ResidentController {
  // Criar morador
  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Erro na validação.' });
    }
    const resident = await Residents.create(req.body);
    return res.json(resident);
  }
}

export default new ResidentController();
