// Yup: Modulo de validação de schemas
import * as Yup from 'yup';

import User from '../models/User';
import Condo from '../models/Condo';

// Regex para o formato do número de telefone
const phoneRegExp = /(\(?\d{2}\)?)?(\d{4,5}\d{4})/;

class SignUpController {
  async store(req, res) {
    // Campos de criação do Condomínio
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      adress: Yup.string().required(),
      phone: Yup.string()
        .required()
        .matches(phoneRegExp),
    });

    // Se o body passado for válido
    if (!(await schema.isValid(req.body.condos))) {
      return res.status(400).json({ error: 'Erro na validação.' });
    }

    // Check se já existe condomínio com o mesmo nome
    const condoExists = await Condo.findOne({
      where: { name: req.body.condos.name },
    });

    if (condoExists) {
      return res
        .status(400)
        .json({ error: 'Nome de condomínio já cadastrado.' });
    }

    const condo = await Condo.create({
      name: req.body.condos.name,
      adress: req.body.condos.adress,
      phone: req.body.condos.phone,
    });

    // Verificação se não já existe email cadastrado
    const userExists = await User.findOne({
      where: { email: req.body.users.email },
    });

    if (userExists) {
      return res.status(400).json({ error: 'Email de usuário já cadastrado.' });
    }

    // Verificação se não já existe login cadastrado
    const loginExists = await User.findOne({
      where: { login: req.body.users.login },
    });

    if (loginExists) {
      return res.status(400).json({ error: 'Nome de login já utilizado' });
    }

    // Campos de criação do Usuário Síndico
    const user = await User.create({
      name: req.body.users.name,
      email: req.body.users.email,
      login: req.body.users.login,
      password: req.body.users.pass,
      password_question: req.body.users.passquestion,
      password_answer: req.body.users.passanswer,
      condominium_id: condo.id,
      profile_id: 1, // 1: Síndico
    });

    return res.json(user);
  }
}

export default new SignUpController();
