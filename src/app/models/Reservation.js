// Manipular os dados dos Condomínios
import Sequelize, { Model } from 'sequelize';

class Reservation extends Model {
  static init(sequelize) {
    super.init(
      // Objeto que contém todos os valores que o usuário pode receber
      {
        // Campos que o usuário poderá preencher
        name: Sequelize.STRING,
        date: Sequelize.DATE,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  // Associar com Morador e Área Comum
  static associate(models) {
    this.belongsTo(models.Resident, {
      foreignKey: 'resident_id',
      as: 'resident',
    });
    this.belongsTo(models.CommonArea, {
      foreignKey: 'common_area_id',
      as: 'common_area',
    });
  }
}
export default Reservation;
