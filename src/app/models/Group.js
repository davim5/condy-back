// Manipular os dados dos Condomínios
import Sequelize, { Model } from 'sequelize';

class Group extends Model {
  static init(sequelize) {
    super.init(
      // Objeto que contém todos os valores que o usuário pode receber
      {
        // Campos que o usuário poderá preencher
        name: Sequelize.STRING,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  // Associar com o condomínio
  static associate(models) {
    this.belongsTo(models.Condo, {
      foreignKey: 'condominium_id',
      as: 'condominium',
    });
  }
}
export default Group;
