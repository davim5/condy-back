// Manipular os dados dos Condomínios
import Sequelize, { Model } from 'sequelize';

class Resident extends Model {
  static init(sequelize) {
    super.init(
      // Objeto que contém todos os valores que o usuário pode receber
      {
        // Campos que o usuário poderá preencher
        name: Sequelize.STRING,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  // Associar com Unidade
  static associate(models) {
    this.belongsTo(models.User, { foreignKey: 'unit_id', as: 'unit' });
  }
}
export default Resident;
