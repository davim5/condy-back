// Manipular os dados dos Users
import Sequelize, { Model } from 'sequelize';

class Profile extends Model {
  static init(sequelize) {
    super.init(
      // Objeto que contém todos os valores que o usuário pode receber
      {
        // Enviar colunas da base de dados através de um Objeto que serão utilizadas
        // Campos que o usuário poderá preencher
        name: Sequelize.STRING,
      },
      {
        // Tem que passar sequelize como parametro
        // Utilizando como conexão com o Banco de Dados
        sequelize,
      }
    );

    return this;
  }
}
export default Profile;
