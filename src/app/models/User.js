// Manipular os dados dos Users
import Sequelize, { Model } from 'sequelize';

// Módulo pra criar o hash da senha
import bcrypt from 'bcryptjs';

class User extends Model {
  static init(sequelize) {
    super.init(
      // Objeto que contém todos os valores que o usuário pode receber
      {
        // Enviar colunas da base de dados através de um Objeto que serão utilizadas
        // Campos que o usuário poderá preencher
        name: Sequelize.STRING,
        email: Sequelize.STRING,
        login: Sequelize.STRING,
        password: Sequelize.VIRTUAL, // Campo só no código
        password_hash: Sequelize.STRING,
        password_question: Sequelize.STRING,
        password_answer: Sequelize.VIRTUAL, // Campo só no código
        password_answer_hash: Sequelize.STRING,
        last_activity: Sequelize.DATE,
        status: Sequelize.INTEGER,
        syndical: Sequelize.BOOLEAN,
        verified: Sequelize.BOOLEAN,
      },
      {
        // Tem que passar sequelize como parametro
        // Utilizando como conexão com o Banco de Dados
        sequelize,
      }
    );

    // Gancho de função
    // Essa será chamada toda vez que o um usuário for salvo
    this.addHook('beforeCreate', async user => {
      // A senha guardada no BD será um Hash da senha passada pelo usuário.
      user.password_hash = await bcrypt.hash(user.password, 8);
      // Resposta da pergunta de recuperação da senha será um Hash também.
      user.password_answer_hash = await bcrypt.hash(user.password_answer, 8);
    });

    return this;
  }

  // Relacionar User com as tabelas File, Profile e Condomínio
  static associate(models) {
    // vai ser armazenado um 'Id' de arquivo no model de 'User'
    this.belongsTo(models.File, { foreignKey: 'avatar_id', as: 'avatar' });
    this.belongsTo(models.Profile, { foreignKey: 'profile_id', as: 'profile' });
    this.belongsTo(models.Condo, {
      foreignKey: 'condominium_id',
      as: 'condominium',
    });
  }

  // Método de checar senha do User
  checkPassword(password) {
    // Comparando senha passada pelo usuário com a senha original
    return bcrypt.compare(password, this.password_hash);
    // retorna true se for igual
  }
}
export default User;
