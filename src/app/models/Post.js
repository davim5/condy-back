// Manipular os dados dos Condomínios
import Sequelize, { Model } from 'sequelize';

class Post extends Model {
  static init(sequelize) {
    super.init(
      // Objeto que contém todos os valores que o usuário pode receber
      {
        // Campos que o usuário poderá preencher
        text: Sequelize.STRING,
        number_like: Sequelize.INTEGER,
        type: Sequelize.INTEGER,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  // Associar com o morador, grupo e com próprio post
  static associate(models) {
    this.belongsTo(models.Resident, {
      foreignKey: 'resident_id',
      as: 'resident',
    });
    this.belongsTo(models.Group, {
      foreignKey: 'group_id',
      as: 'group',
    });
    this.belongsTo(models.Post, {
      foreignKey: 'post_parent',
      as: 'parent',
    });
  }
}
export default Post;
