// Manipular os dados dos Condomínios
import Sequelize, { Model } from 'sequelize';

class CommonArea extends Model {
  static init(sequelize) {
    super.init(
      // Objeto que contém todos os valores que o usuário pode receber
      {
        // Campos que o usuário poderá preencher
        name: Sequelize.STRING,
        open_hour: Sequelize.DATE,
        close_hour: Sequelize.DATE,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  // Associar com o condomínio
  static associate(models) {
    this.belongsTo(models.Condo, {
      foreignKey: 'condominium_id',
      as: 'condominium',
    });
  }
}
export default CommonArea;
