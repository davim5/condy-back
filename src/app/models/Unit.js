// Manipular os dados dos Condomínios
import Sequelize, { Model } from 'sequelize';

class Unit extends Model {
  static init(sequelize) {
    super.init(
      // Objeto que contém todos os valores que o usuário pode receber
      {
        // Enviar colunas da base de dados através de um Objeto que serão utilizadas
        // Campos que o usuário poderá preencher
        name: Sequelize.STRING,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  // Associar com Unidade
  static associate(models) {
    this.belongsTo(models.Condo, {
      foreignKey: 'condominium_id',
      as: 'condominium',
    });
  }
}
export default Unit;
