// Manipular os dados dos Condomínios
import Sequelize, { Model } from 'sequelize';

class Condo extends Model {
  static init(sequelize) {
    super.init(
      // Objeto que contém todos os valores que o usuário pode receber
      {
        // Campos que o usuário poderá preencher
        name: Sequelize.STRING,
        adress: Sequelize.STRING,
        phone: Sequelize.STRING,
        status: Sequelize.INTEGER,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  // Relacionar Condo com User
  // static associate(models) {
  //   this.belongsTo(models.User, { foreignKey: 'syndic_id', as: 'syndic' });
  // }
}
export default Condo;
