// Manipular os dados dos Condomínios
import Sequelize, { Model } from 'sequelize';

class Employee extends Model {
  static init(sequelize) {
    super.init(
      // Objeto que contém todos os valores que o usuário pode receber
      {
        // Campos que o usuário poderá preencher
        name: Sequelize.STRING,
        job: Sequelize.INTEGER,
        schedule: Sequelize.DATE,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  // Associar com o condomínio e unidade
  static associate(models) {
    this.belongsTo(models.User, { foreignKey: 'unit_id', as: 'unit' });
    this.belongsTo(models.Condo, {
      foreignKey: 'condominium_id',
      as: 'condominium',
    });
  }
}
export default Employee;
