// Import do arquivo .env, contendo configurações
import 'dotenv/config';

import express from 'express';
import 'express-async-errors';
import path from 'path';
// Dá um tratamento melhor nas mensagem de erro para desenvolvedor
import Youch from 'youch';
// Tratamento de exceções
import * as Sentry from '@sentry/node';
// Import da configuração do Sentry
import sentryConfig from './config/sentry';
// Import do arquivo das rotas
import routes from './routes';

// Chamar o index do database
import './database';

class App {
  constructor() {
    this.server = express();

    Sentry.init(sentryConfig);
    this.middlewares();
    this.routes();
    this.exceptionHandler();
  }

  middlewares() {
    this.server.use(Sentry.Handlers.requestHandler());
    this.server.use(express.json());

    // Acessar arquivos estáticos
    this.server.use(
      '/files',
      express.static(path.resolve(__dirname, '..', 'tmp', 'uploads'))
    );
  }

  routes() {
    this.server.use(routes);
    this.server.use(Sentry.Handlers.errorHandler());
  }

  // Middleware de tratamento de exceções
  exceptionHandler() {
    this.server.use(async (err, req, res, next) => {
      // Se for ambiente de desenvolvimento, retorna detalhes do erro
      if (process.env.NODE_ENV === 'development') {
        const errors = await new Youch(err, req).toJSON();

        // Internal Server Error
        return res.status(500).json(errors);
      }

      // Retorna apenas o tipo de erro.
      return res.status(500).json({ error: 'Internal Server Error' });
    });
  }
}
export default new App().server;
