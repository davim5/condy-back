import { Router } from 'express';

// Módulo do envio de arquivo e config
import multer from 'multer';
import multerConfig from './config/multer';

// Controllers
import UserController from './app/controllers/UserController';
import SessionController from './app/controllers/SessionController';
import FileController from './app/controllers/FileController';
import SyndicController from './app/controllers/SyndicController';
import CondoController from './app/controllers/CondoController';
import GroupController from './app/controllers/GroupController';
import EmployeeController from './app/controllers/EmployeeController';
import ResidentController from './app/controllers/ResidentController';
import PostController from './app/controllers/PostController';
import UnitController from './app/controllers/UnitController';
import CommonAreaController from './app/controllers/CommonAreaController';
import ReservationController from './app/controllers/ReservationController';
import VerificationController from './app/controllers/VerificationController';
import SignUpController from './app/controllers/SignUpController';

import ProfileController from './app/controllers/ProfileController';
import RoleController from './app/controllers/RoleController';

// Middlewares
// middleware de autenticação do token da sessão
import authMiddleware from './app/middlewares/auth';
// middleware de verificação do status 'verified' do usuário na sessão
import verifyMiddleware from './app/middlewares/verify';

const routes = new Router();
const upload = multer(multerConfig);

/* ROTAS DE PROCESSOS */
routes.post('/signup', SignUpController.store);

/*    ROTAS PROFILES     */
routes.post('/profiles', ProfileController.store);
routes.delete('/profiles', ProfileController.delete);

/*     ROTAS ROLES      */
routes.post('/roles', RoleController.store);

/*     ROTAS USUÁRIO      */
// Criar Usuário
routes.post('/users', UserController.store);
// Mostrar Usuários
routes.get('/users', authMiddleware, verifyMiddleware, UserController.index);
// Atualizar Usuário
routes.put('/users', authMiddleware, UserController.update);
// Deletar Usuários
routes.delete('/users', UserController.delete);
// Atualizar Status para Ativo
routes.put('/users/status', authMiddleware, UserController.updateStatusActive);

/*    ROTAS GRUPOS    */
// Criar grupo
routes.post('/groups', authMiddleware, GroupController.store);
// Listar grupos pelo condomínio do usuário logado
routes.get('/groups', authMiddleware, GroupController.index);

/*    ROTAS POSTS     */
routes.post('/posts', PostController.store);
routes.post('/start', authMiddleware, PostController.welcomePost);
routes.get('/posts', authMiddleware, PostController.index);

/*   ROTAS LOGIN     */
// Criar Sessão
routes.post('/sessions', SessionController.store);

/*   ROTAS DE EMAIL E VERIFICAÇÃO  */
// Enviar email
routes.get('/send', authMiddleware, VerificationController.sendMail);
// Verificar email
routes.get('/verify', authMiddleware, VerificationController.verifyMail);

/*   ROTAS CONDOMÍNIOS     */
// Criar Condomínio
routes.post('/condos', CondoController.store);
// Atualizar Status do Condomínio para ativo
routes.put('/condos', authMiddleware, CondoController.updateStatusActive);
// Get Info do Condomínio
routes.get('/condos', authMiddleware, CondoController.index);
// Deletar Condomínio
routes.delete('/condos', CondoController.delete);

// Enviar Arquivo
routes.post(
  '/files',
  authMiddleware,
  upload.single('file'),
  FileController.store
);

/*    ROTAS FUNCIONÁRIOS     */
// Criar Funcionário
routes.post('/employees', authMiddleware, EmployeeController.store);

/*     ROTAS RESIDENTES      */
// Criar residente
routes.post('/residents', authMiddleware, ResidentController.store);

/*     ROTAS UNIDADES    */
// Criar unidade
routes.post('/units', UnitController.store);
// Criar unidade no condomínio do Usuário
routes.post('/user/units', authMiddleware, UnitController.storeByUser);

/* ROTAS ÁREA COMUM */
// Criar àrea comum
routes.post('/commonareas', authMiddleware, CommonAreaController.store);
routes.get('/commonareas', authMiddleware, CommonAreaController.index);

/* ROTAS RESERVAS */
routes.post('/reservations', ReservationController.store);
routes.get('/reservations', authMiddleware, ReservationController.index);

// Mostrar Síndicos
routes.get('/syndics', authMiddleware, SyndicController.index);

export default routes;
