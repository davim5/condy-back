module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.addColumn('posts', 'post_parent', {
      type: Sequelize.INTEGER,
      references: { model: 'posts', key: 'id' },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
      allowNull: true,
    }),

  down: queryInterface => queryInterface.removeColumn('posts', 'post_parent'),
};
