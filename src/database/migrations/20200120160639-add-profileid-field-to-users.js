module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.addColumn('users', 'profile_id', {
      type: Sequelize.INTEGER,
      references: { model: 'profiles', key: 'id' },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
      allowNull: true,
    }),

  down: queryInterface => queryInterface.removeColumn('users', 'profile_id'),
};
