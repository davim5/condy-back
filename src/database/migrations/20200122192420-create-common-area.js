module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('common_areas', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      condominium_id: {
        type: Sequelize.INTEGER,
        references: { model: 'condos', key: 'id' },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        allowNull: false,
      },
      open_hour: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      close_hour: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    }),

  down: queryInterface => queryInterface.dropTable('common_areas'),
};
