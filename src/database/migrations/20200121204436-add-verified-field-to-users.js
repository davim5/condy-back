module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.addColumn('users', 'verified', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    }),

  down: queryInterface => queryInterface.removeColumn('users', 'verified'),
};
