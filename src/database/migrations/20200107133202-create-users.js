module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('users', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      login: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      email_confirmed: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      password_hash: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      password_question: {
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      password_answer_hash: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      /*
      status:
      1 - New
      2 - Active
      3 - Blocked
      */
      status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1,
      },
      syndical: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      last_activity: {
        // Atualizar sempre que fizer o login
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: new Date(),
      },
    }),

  down: queryInterface => queryInterface.dropTable('users'),
};
