module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.addColumn('users', 'condominium_id', {
      type: Sequelize.INTEGER,
      references: { model: 'condos', key: 'id' },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
      allowNull: true,
    }),

  down: queryInterface =>
    queryInterface.removeColumn('users', 'condominium_id'),
};
