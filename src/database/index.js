import Sequelize from 'sequelize';
// Importando configurações do BD
import databaseConfig from '../config/database';

// Import dos Models
import User from '../app/models/User';
import File from '../app/models/File';
import Profile from '../app/models/Profile';
import Role from '../app/models/Role';
import Condo from '../app/models/Condo';
import Group from '../app/models/Group';
import Employee from '../app/models/Employee';
import Resident from '../app/models/Resident';
import Post from '../app/models/Post';
import Unit from '../app/models/Unit';
import CommonArea from '../app/models/CommonArea';
import Reservation from '../app/models/Reservation';

// Array contendo todos os Models da aplicação
const models = [
  User,
  File,
  Condo,
  Employee,
  Group,
  Profile,
  Role,
  Resident,
  Post,
  Unit,
  CommonArea,
  Reservation,
];

class Database {
  constructor() {
    this.init();
  }

  init() {
    // Estabelecendo conexão com Base de Dados
    // Variável utilzada pelos Models
    this.connection = new Sequelize(databaseConfig);

    // Percorrer array de models
    // De cada model, chamar o método 'init' passando a conexão
    models
      .map(model => model.init(this.connection))
      // Rodar método associate se tiver esse método
      .map(model => model.associate && model.associate(this.connection.models));
  }
}
export default new Database();
