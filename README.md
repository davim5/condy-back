# Insomnia Settings

* Base Evironment do Insomnia
```
{
  "base_url": "http://localhost:3333",
  "token": " 'token gerado pela Session' "
}
```
## Rotas

### Condos
##### Create Condo (WS1.1)
* POST {{ base_url  }}/condos
```
 {
	  "name": "Condomínio Jardim Verde",
    "adress":"Rua Gramado",
    "phone": "39999999"
 }
```
##### Update Condo Status to Active (WS3.1)
* PUT {{ base_url  }}/condos

##### List Condominium by User in Session (WS7)
* GET {{ base_url  }}/condos

##### Delete Condominium by id
* DELETE {{ base_url  }}/condos
```
{
	"id":1
}
```

### Groups
##### Create Group (WS1.3)
* POST {{ base_url  }}/groups
```
{
	"name":"Moradores",
	"condominium_id": 1
}
```
##### Get Groups by Condominium in Users Session (WS11)
* POST {{ base_url  }}/groups

### Users
##### Create User (WS1.2) & (WS2)
* POST {{ base_url  }}/users
```
{
	"name": "Davi Lima",
  "email": "DaviL@gmail.com",
	"login": "davim5",
  "password": "123456",
	"password_question": "Qual minha banda favorita?",
	"password_answer": "The Beatles",
	"condominium_id":1,
	"profile_id":1
}
```
##### Get User in Session Info (WS5)
* GET {{ base_url  }}/users

##### Update User Status to Active (WS3.2)
* PUT {{ base_url  }}/users/status

##### Delete User by id
* DELETE {{ base_url  }}/users
```
{
	"id":1
}
```

### Units
##### Create Unit
* POST {{ base_url  }}/groups
```
{
	"name":"302",
	"condominium_id": 1
}
```

### Profiles
##### Create Profile
* POST {{ base_url  }}/profiles
```
{
	"name":"Adm Condomínio"
}
```

### Sessions
##### Create Session (WS4)
* POST {{ base_url  }}/sessions
```
{
	"login":"davim5",
	"password":"123456"
}
```
### Employees
##### Create Employee
* POST {{ base_url  }}/employees
```
{
	"name": "edmilson",
	"job": 1,
	"condominium_id":1,
	"unit_id":1
}
```
### Residents
##### Create Resident
* POST {{ base_url  }}/residents
```
{
	"name":"Kleber",
	"unit_id":1
}
```
### Posts
##### Create Post
* POST {{ base_url  }}/posts
```
{
	"text":"Bem vindo ao Condy! Comece agora mesmo a interagir com todos os seus vizinhos desse lugar!",
	"type":1,
	"resident_id":1,
	"group_id": 1,
	"post_parent": 1
}
```
##### Create Welcome Post on Group of Condominium by User Session (WS3.3)
* POST {{ base_url  }}/start

##### List all Posts on Group of Condominium by User Session (WS11)
* GET {{ base_url  }}/posts


### Roles
##### Create Role
* POST {{ base_url  }}/roles
```
{
	"name":"Administração",
	"permission": false
}
```

### Common Areas
##### Create Common Area
* POST {{ base_url  }}/commonareas
```
{
	"name":"Briquedoteca",
	"condominium_id": 1
}
```

##### List all Common Area by Condominium by User Session (WS8)
* GET {{ base_url  }}/commonareas

### Reservations
##### Create Reservation
* POST {{ base_url  }}/reservations
```
{
	"name": "Aniversário do Davi",
	"date":"2020-02-18T10:00:00-03:00",
	"resident_id":1,
	"common_area_id":1
}
```

##### List all Reservations by Common Area by Condominium by User Session (WS8)
* GET {{ base_url  }}/commonareas

# How to Start

## Docker

``docker run --name condydb -e POSTGRES_PASSWORD=docker -p 5432:5432 -d postgres:11``

## Sequelize

``yarn sequelize db:migrate``

### WS1

1. Criar Condo
2. Criar Profile
3. Criar User
  - Com condomínio id do item 1
  - Com profile id do item 2
4. Criar Group
  - Com condomínio id do item 1
5. Criar Session
  - Com login e senha do Usuário criado
6. Colocar token gerado no item 5 no Base Enviromnent do Insomnia
7. Criar Unit
  - Com condomínio id do item 1
8. Criar Employee
  - Com condomínio id do item 1
  - Com Unit id do item 7

### WS2

* O email é enviado ao criar o Usuário, mas o link de ativação ainda não foi implementado

### WS3

* Obs: Essas rotas funcionam apenas com o token da Session iniciada

1. Mudar Status do Condomínio (WS3.1)
2. Mudar Status do Usuário (WS3.2)
3. Criar Post de Bem vindo (WS3.3)

### WS4

* Rota da Session (WS4)
* Quando o link de ativação estiver funcionando, vai ter uma troca de parâmetro no usuário
que vai permitir o acesso às próximas WS quando for 'true'.





